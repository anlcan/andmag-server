from django.utils import simplejson as json
from types import InstanceType


class A():

    def __init__(self):
        self.k = ('tuple1', 'tuple2')

class Handler():
    """
    [int]
    str username:
    int userId:
    [out]:
    bool result:
    """
    def __init__(self):
        self.a = 1
        self.b = 'str'
        self.c = 0.2
        self.d = ['list', 'list2']
        #self.e = {'key':'value'}
        self.bob = A()

    def run(self):

        print("runs very fast")


TYPE = "_type"
SAFE_TYPES = (int, float, str, unicode)
ITER_TYPE  = (list, tuple)

def serValue(value):

    if  type(value) in SAFE_TYPES:
        return  value

    elif type(value) == InstanceType:
        return _serialize(value)

    elif type(value) in ITER_TYPE:
        l = list()
        for it in value:
            l.append(serValue(it))

        return l

    elif type(value) == dict:
        raise Exception("Cannot send dictionary objects. Make classes or wait for new version")

    else:
        print('unknown obj ', type(value), value)
        return None

def _serializeDict(dictionary):

    result = dict()

    for key,value in dictionary.iteritems():
        v = serValue(value)
        if  v:
            result[key] = v

    return result

def _serialize(obj):
    print ( "object:",obj)
    dictionary = vars(obj)
    result = _serializeDict(dictionary)
    result[TYPE] = obj.__class__.__name__
    return result

def serialize(obj):
    d = _serialize(obj)
    print ( "serialized :",d)
    return json.dumps(d)

##########
def deserVal(value):

    if type(value) == dict:
       return _deserialize(value)
    elif type(value) in SAFE_TYPES:
        return value
    elif type(value) in ITER_TYPE:
        l = list()
        for i in value:
            list.append(deserVal(i))
        return l
    else:
        print('unknown obj ', type(value), value)
        return None


def _deserialize(dic):

    print ( "_des",dic)
    className = dic[TYPE]
    inst = globals()[className]()

    for key, value in dic.iteritems():
        v = deserVal(value)
        if v:
            setattr(inst, key,v)

    return inst


def deserialize(str):
    return _deserialize(loads(dic))


if __name__ =="__main__":
    k  = serialize(Handler())
    print (k)
    l = deserialize(k)
    print ( l)

    ll = '{"cityUrlName":"dubai","pager":{"pageNumber":1,"pageSize":40,"_type":"RemotePager"},"_type":"GetAllDealsRequest","session":"66947F5F-902F-4832-95D1-2208694FABF0","registerId":"031FBE1.F3A"}'
    request = deserialize(ll)

    print (request)
    


