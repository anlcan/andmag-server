from google.appengine.api import urlfetch
from boto.s3.connection import  S3Connection
from boto.s3.key import  Key

#anilcan
#AWS_ACCESS_KEY_ID = "AKIAJ7LDNA4Z6PWO2IKQ"
#AWS_SECRET_ACCESS_KEY = "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"
##test
#BUCKET="andmag_test"


#dukkan  creative
AWS_ACCESS_KEY_ID = "AKIAJTL5AKTHQTVKAX7A"
AWS_SECRET_ACCESS_KEY = "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"


BUCKET="travelapp"

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp import logging
from google.appengine.ext import db

import json

# d'oh
# dont use "." as separator as https will fail..

FREE_ISSUE=['120903_1', '120831_1']
DEBUG=True

class Purhcase(db.Model):
    purchaseDate = db.DateTimeProperty(auto_now=True)
    identifier = db.StringProperty()
    receipt = db.StringProperty()

class Issue (db.Model):

    name = db.StringProperty()
    description = db.TextProperty()
    identifier =  db.StringProperty()
    environment = db.StringProperty()
    lastUpdate = db.DateTimeProperty(auto_now_add=True)
    price = db.StringProperty()
    cover = db.BlobProperty()
    cover2= db.BlobProperty()
    cover3= db.BlobProperty()
    current = db.BooleanProperty()

    longitude=db.IntegerProperty()
    latitude=db.IntegerProperty()

def fetchIssue(name):

    query = db.Query(Issue)
    # this here OBLIGES the identifier==Directory name rule!
    query.filter('identifier = ', name)

    results = query.fetch(limit=1)
    for r in results:
        return r

    issue = Issue(identifier=name)

    issue.put()

    return issue

class BaseHandler(webapp.RequestHandler):
    def get(self):
        pass

class ListIssues(BaseHandler):

    def get(self):
        # should I keep the connection?
        connection = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
        
        bucket = connection.get_bucket(BUCKET)
        keys = bucket.list()

        tmp = list()


        for key  in keys :
            if key.name.endswith("/"):
                key.secret = key.name[:-1]
                tmp.append(key)

        self.response.out.write(template.render('templates/Issues.html', {"keys":tmp}))


class RemoveIssue(BaseHandler):

    def get(self):
        issueName = self.request.url.split("/")[-2]
        issue = fetchIssue(issueName)

        issue.delete()


class EditIssue(BaseHandler):

    def get(self):

        issueName = self.request.url.split("/")[-2]
        issue = fetchIssue(issueName)


        description_value = issue.description
        id_value = issue.identifier
        name_value = issue.name
        env = issue.environment
        price_value = issue.price
        current = issue.current
        isProduction = "checked"
        isTest = "checked"

        url = "img?img_id=%s" % issue.key()


        if env == "Production":
            isTest = ""
        else :
            isProduction = ""

        isCurrent = ""
        if current :
            isCurrent = "checked"

        logging.info("-------- wtf get -------------")
        logging.info("id: %s", id_value)
        logging.info("desc: %s", description_value)
        logging.info("environment: %s", env)

        self.response.out.write(template.render('templates/Edit.html', {"issueName": issueName,
                                                                        "kapakUrl" : url,
                                                                        "description_value":description_value,
                                                                        "id_value":id_value,
                                                                        "name_value":name_value,
                                                                        "isProduction":isProduction,
                                                                        "isTest":isTest,
                                                                        "price_value":price_value,
                                                                        "isCurrent":isCurrent,
                                                                        "latitude_value" : issue.latitude,
                                                                        "longitude_value" : issue.longitude

                                                                        }))
    def post(self):

        id = self.request.get('id')
        description = self.request.get('description')
        environment = self.request.get('environment')
        price = self.request.get('price')
        current =self.request.get('current')
        name =  self.request.get("name")
        cover = self.request.get("img")
        cover2 = self.request.get("img2")
        cover3 = self.request.get("img3")


        logging.info("-------- wtf post-------------")
        logging.info("id: %s", id)
        logging.info("desc: %s", description)
        logging.info("environment: %s", environment)
        logging.info("current: %s", current)
        #logging.info("img: %s", cover)

        issueName = self.request.url.split("/")[-2]

        issue = fetchIssue(issueName)
        issue.description = description
        issue.identifier = id
        issue.environment = environment
        issue.price = price
        issue.current = (current == "current")
        issue.name = name
        issue.latitude  = int(self.request.get('latitude'))
        issue.longitude = int(self.request.get('longitude'))

        if cover :
            issue.cover = db.Blob(cover)

        if cover2 :
            issue.cover2 = db.Blob(cover2)

        if cover3 :
            issue.cover3 = db.Blob(cover3)


        issue.put()

        self.redirect("/admin/?msg=Issue%20saved!")

        

class GetProducts(BaseHandler):
    def get(self):

        issues = list()
        query = Issue.all()
        #HERE BE DRAGONS....fix it

        results = query.fetch(limit=20)
        #results = None

        env = self.request.headers['x-application-env']
        for issue in results:

            if env == 'Production' and issue.environment == 'Test':
                continue


            issues.append({"_type":"Product",
                           "identifier": issue.identifier,
                           "description": issue.description,
                           "price" : issue.price,
                           "cover_key" :str(issue.key()),

                           "isCurrent": issue.current,
                           "latitude" : issue.latitude,
                           "longitude" : issue.longitude,
                           "name": issue.name})

        issues.reverse()

        self.response.out.write(json.dumps(issues))

class GetImage(BaseHandler):
    def get(self):

        imageName = self.request.url.split("/")[-2]
        logging.info("imageName:" + imageName)

        connection = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
        bucket = connection.get_bucket(BUCKET)

        cover_key = bucket.get_key(imageName+"/kapak.jpg")
        if cover_key:
            url = cover_key.generate_url(60 * 5)
            self.response.out.write(json.dumps({"url": url}))
            logging.info("url %s", url)
        else :
            logging.info("missing key "+ imageName+"/kapak.jpg")
            self.response.out.write({})

class GetIssue(BaseHandler):

    def verify(self, receipt, isDebug=False):
        """
        21000 The App Store could not read the JSON object you provided.
        21002 The data in the receipt-data property was malformed.
        21003 The receipt could not be authenticated.
        21004 The shared secret you provided does not match the shared secret on file for your account.
        21005 The receipt server is not currently available.
        21006 This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.
        21007 This receipt is a sandbox receipt, but it was sent to the production service for verification.
        21008 This receipt is a production receipt, but it was sent to the sandbox service for verification.

        """
        #http://developer.apple.com/library/ios/#documentation/NetworkingInternet/Conceptual/StoreKitGuide/VerifyingStoreReceipts/VerifyingStoreReceipts.html#//apple_ref/doc/uid/TP40008267-CH104-SW1
        debugURL = "https://sandbox.itunes.apple.com/verifyReceipt"
        storeURL = "https://buy.itunes.apple.com/verifyReceipt"

        url = storeURL
        if isDebug : url = debugURL

        logging.info ("verifying %s on %s" %(url, receipt))

        form_fields = { "receipt-data" : receipt }

        resultRaw = urlfetch.fetch(url=url,
                            payload=json.dumps(form_fields),
                            method=urlfetch.POST,
                            headers={'Content-Type': 'text/json'})

        logging.info("received response %s" % resultRaw.content)

        result = json.loads(resultRaw.content)
        logging.info("received response %s" % resultRaw.content)


        return result["status"] == 0

    def post(self):

        issueName = self.request.url.split("/")[-2]
        logging.info("download issueName:" + issueName)

        body = self.request.body
        params = json.loads(body)
        receipt = params['receipt']
        environment = params['environment']


        # client should send environment param SANDBOX|DEBUG for debugging
        # else, we must fail with an error
        logging.info("params %s", params)

        isDebug = False
        if environment in ['SANDBOX', 'DEBUG']:
            isDebug = True

#        if not issueName in FREE_ISSUE: # MOSTLY FOR DEBUGGING PURPOSES
#            verification = self.verify(receipt,isDebug )
##
#        if not verification:
#            logging.info("verification failed! Returning NOK-500")
#            self.response.clear()
#            self.response.set_status(501)
#            self.response.out.write("Verification failed!")
#            return


        connection = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
        bucket = connection.get_bucket(BUCKET)

        cover_key = bucket.get_key(issueName+"/content.zip")
        #cover_key = bucket.get_key(imageName+"/test.pdf")

        if cover_key:
            url = cover_key.generate_url(60 * 15)
            self.response.out.write(json.dumps({"url": url}))
            logging.info("url %s", url)
        else :
            logging.info("missing key "+ issueName+"/content.pdf")
            self.response.out.write({})

class Image(webapp.RequestHandler):

    def get(self):
        issue = db.get(self.request.get("img_id"))
        if issue:

            imgName = str(self.request.get("img_name"))

            if issue.__getattribute__(imgName):
                self.response.headers['Content-Type'] = "image/png"
                self.response.out.write(issue.__getattribute__(imgName))
            else:
                self.response.out.write("No image")
                self.response.set_status(500)
        else:
            self.response.out.write("No issue")
            self.response.set_status(500)


application = webapp.WSGIApplication([
                                        ('/admin/', ListIssues),
                                        ('/admin/edit/[^/]*/', EditIssue),
                                        ('/products', GetProducts),
                                        ('/getImage/[^/]*/', GetImage),
                                        ('/getIssue/[^/]*/', GetIssue),

                                        ('/img', Image)
                                        ],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()